FROM node:alpine

WORKDIR /app

COPY package*.json ./
COPY app.js ./

RUN npm install

EXPOSE 2375

CMD ["node", "start"]